"use strict";

var name = 'Garneau';
var age = 50;
var isCool = true;

function user(userName, userAge, userIsCool) {
 return ( 'Name : ' + userName + ', age : ' + userAge +
 ' and is the user cool ? ' + userIsCool );
}

console.log(user(name, age, isCool));

// ========================================================

let userArrow = (userName, userAge, userIsCool) => {
  return ( 'Name : ' + userName + ', age : ' + userAge +
 ' and is the user cool ? ' + userIsCool );
};

console.log(userArrow(name, age, isCool));

// ========================================================

// Si la fonction prend un seul paramètre, on peut omettre les parenthèses
let addToOne = a => {
  return a + 1;
};

console.log('addToOne', addToOne(2)); // 3

// ========================================================

// Si la fonction utilise seulement un return, on peut utiliser 
// la version courte sur une ligne
let addToOneSimple = a => a + 1;

console.log('addToOneSimple', addToOneSimple(2)); // 3

// ========================================================

// Si la fonction ne prend pas de paramètres, il faut mettre
// des parenthèses vides
let GiveMeRandom = () => Math.random();

console.log('GiveMeRandom', GiveMeRandom()); // Random number
