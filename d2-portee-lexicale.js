"use strict";

var foo = "bar"; // globale, visible partout

function test() {
  console.log(foo); // "bar"
  if (true){
    var baz = "var local"; // visible dans la fonction
    let fez = "let local"; // visible dans le block if
  }
  console.log(baz); // "var local"
  // console.log(fez); // erreur
}

test();

console.log(foo); // "bar"
// console.log(baz); // erreur