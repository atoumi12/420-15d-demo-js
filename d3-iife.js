"use strict";

// Fonction immédiatement invoquée
(function() {
  console.log("dans la fonction");
})();

// ========================================================

// On peut passer des paramètres
(function(i, j) {
  let k = i + j;
  console.log("dans la fonction => " + k);
})(2, 3);

// ========================================================

// Les variables déclarées ne "polluent" pas le scope
(function() {
  var foo = "bar";
  console.log("dans la fonction 2");
})();

// console.log(foo); // undefined

// ========================================================

let app = (function() {
  let id = 123;
  console.log("dans la fonction app => " + id);
  return id;
})();

console.log('app', app); // 123